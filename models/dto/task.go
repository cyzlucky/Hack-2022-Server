package dto

type TaskExportMode struct {
	Name string `json:"name"`
	Id   int    `json:"id"`
}

type TaskMode struct {
	Name string `json:"name"`
	Id   int    `json:"id"`
}
